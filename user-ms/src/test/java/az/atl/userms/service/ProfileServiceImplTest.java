package az.atl.userms.service;

import az.atl.userms.dao.entity.UserEntity;
import az.atl.userms.dao.repository.UserRepository;
import az.atl.userms.exception.NoSuchUserException;
import az.atl.userms.model.dto.UserDto;
import az.atl.userms.service.impl.ProfileServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
public class ProfileServiceImplTest {

    @Mock
    private UserRepository userRepository;


    @InjectMocks
    private ProfileServiceImpl profileService;


    @Test
    void testGetUserById_withTrueCase(){
        UserEntity entity = UserEntity.builder()
                .id(1L)
                .firstName("name")
                .lastName("lastname")
                .username("username")
                .build();

        UserDto dto = UserDto.builder()
                .id(1L)
                .name("name")
                .surname("lastname")
                .username("username")
                .build();

        when(userRepository.findById(1L)).thenReturn(Optional.of(entity));
        UserDto result = profileService.getUserById(1L);

        Assertions.assertEquals(dto,result);
    }

    @Test
    void testGetUserById_withException(){
        when(userRepository.findById(any())).thenReturn(Optional.empty());

        Assertions.assertThrows(NoSuchUserException.class,()-> profileService.getUserById(any()));
    }

    @Test
    void createUser_withTrueCase(){
        Long userId = 1L;
        UserEntity user = userRepository.findById(userId).get();

        user.setFirstName("ali");
        user.setLastName("ibrahimzade");

        UserEntity update = userRepository.save(user);

        Assertions.assertEquals("ali",update.getFirstName());
        Assertions.assertEquals("ibrahimzade",update.getLastName());
    }
//    @Test
//    void createProduct_withTrueCase(){
//        var productEntity = ProductEntity.builder()
//                .id(1L)
//                .name("name").build();
//        var productDto = ProductDto.builder()
//                .id(1L)
//                .name("name").build();
//
//        when(productRepository.save(any(ProductEntity.class))).thenReturn(productEntity);
//
//        productService.createProduct(productDto);
//
//        Assertions.assertEquals(productEntity, productDto);
//    }

    @Test
    public void testDeleteUser() {
//        Long userId = 1L; // Assume a user with ID 1 exists
//        userRepository.deleteById(userId);
//
//        UserEntity user = userRepository.findById(userId).get();
//
//        Assertions.assertNull(user);

        profileService.deleteUserById(any());
        verify(userRepository, times(1)).deleteById(any());
    }


}
