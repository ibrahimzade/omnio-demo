package az.atl.userms.service.impl;

import az.atl.userms.dao.entity.UserEntity;
import az.atl.userms.dao.repository.UserRepository;
import az.atl.userms.exception.InvalidPasswordException;
import az.atl.userms.exception.NoSuchUserException;
import az.atl.userms.exception.PasswordNotMatchesException;
import az.atl.userms.model.dto.UpdateDto;
import az.atl.userms.model.dto.UserDto;
import az.atl.userms.service.ProfileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static az.atl.userms.model.consts.ExceptionMessages.*;
import static az.atl.userms.model.mapper.UserMapper.INSTANCE;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProfileServiceImpl implements ProfileService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    private final MessageSource messageSource;


    @Override
    public UserDto getUserById(Long id) {
//        Locale locale = LocaleContextHolder.getLocale();
//        Objects[] args = new Objects[1];
//        String message = messageSource.getMessage()
        Optional<UserEntity> entity = userRepository.findById(id);
        return entity.map(INSTANCE::buildEntityToDto)
                .orElseThrow(() -> new NoSuchUserException(USER_NOT_FOUND.getMessage()));
    }

    @Override
    public Page<UserDto> getAllUsers(Pageable pageable) {
        return userRepository.findAll(pageable).map(INSTANCE::buildEntityToDto);
    }

    @Override
    public void updateUser(UserDto userDto) {
        UserEntity userEntity = INSTANCE.buildDtoToEntity(userDto);
        userRepository.save(userEntity);
        log.info("User is updated");
    }

    @Override
    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
        log.info("User is deleted");
    }
    @Override
    public void changePassword(UpdateDto updateDto){
        UserEntity user = userRepository.findByUsername(updateDto.getUsername()).orElse(null);

        if (user == null){
            throw new NoSuchUserException(USER_NOT_FOUND.getMessage());
        }

        if (!user.getPassword().equals(updateDto.getCurrentPassword())){
            throw new InvalidPasswordException(INVALID_PASSWORD.getMessage());
        }

        if (!updateDto.getNewPassword().equals(updateDto.getCurrentNewPassword())){
            throw new PasswordNotMatchesException(PASSWORD_NOT_MATCHES.getMessage());
        }

        user.setPassword(updateDto.getNewPassword());
        userRepository.save(user);


//        changePassword(updateDto);
//        if ()) {
//            return ResponseEntity.ok("Password changed successfully.");
//        } else {
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Password change failed. Please check your inputs.");
//        }
    }
}
