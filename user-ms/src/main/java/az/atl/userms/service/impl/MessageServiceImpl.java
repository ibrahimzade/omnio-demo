package az.atl.userms.service.impl;

import az.atl.userms.dao.entity.MessageEntity;
import az.atl.userms.dao.entity.UserEntity;
import az.atl.userms.dao.repository.MessageRepository;
import az.atl.userms.dao.repository.UserRepository;
import az.atl.userms.exception.NoSuchUserException;
import az.atl.userms.model.MessageResponse;
import az.atl.userms.model.SendMessageRequest;
import az.atl.userms.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static az.atl.userms.model.consts.ExceptionMessages.USER_NOT_FOUND;

@Service
@AllArgsConstructor
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;
    private final UserRepository userRepository;


    @Override
    public void sendMessage(SendMessageRequest request, UserEntity sender) {
        UserEntity receiver = userRepository.findById(request.getReceiverId())
                .orElseThrow(() ->  new NoSuchUserException(USER_NOT_FOUND.getMessage()));

        MessageEntity messageEntity = MessageEntity.builder()
                .sender(sender)
                .receiver(receiver)
                .content(request.getContent())
                .createdAt(LocalDateTime.now())
                .build();

        messageRepository.save(messageEntity);
    }

    @Override
    public List<MessageResponse> getMessages(Long userId) {
        List<MessageEntity> messages =messageRepository.findBySender_IdOrReceiver_Id(userId , userId);

        return messages.stream()
                .map(messageEntity -> new MessageResponse(
                        messageEntity.getSender().getUsername(),
                        messageEntity.getContent()
                ))
                .collect(Collectors.toList());
    }
}
