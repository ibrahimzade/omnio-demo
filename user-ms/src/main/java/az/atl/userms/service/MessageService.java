package az.atl.userms.service;

import az.atl.userms.dao.entity.UserEntity;
import az.atl.userms.model.MessageResponse;
import az.atl.userms.model.SendMessageRequest;

import java.util.List;

public interface MessageService {
    void sendMessage(SendMessageRequest request, UserEntity sender);

    List<MessageResponse> getMessages(Long userId);
}
