package az.atl.userms.service;

import az.atl.userms.model.dto.UpdateDto;
import az.atl.userms.model.dto.UserDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProfileService {
    Page<UserDto> getAllUsers(Pageable pageable);

    void updateUser(UserDto userDto);

    UserDto getUserById(Long id);

    void deleteUserById(Long id);

    void changePassword(UpdateDto updateDto);

}
