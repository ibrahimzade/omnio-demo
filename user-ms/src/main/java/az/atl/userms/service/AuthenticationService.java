package az.atl.userms.service;

import az.atl.userms.dao.entity.UserEntity;
import az.atl.userms.dao.repository.UserRepository;
import az.atl.userms.exception.NoSuchUserException;
import az.atl.userms.exception.UserAlreadyExistException;
import az.atl.userms.model.AuthenticationRequest;
import az.atl.userms.model.AuthenticationResponse;
import az.atl.userms.model.RegisterRequest;
import az.atl.userms.model.RegisterResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthenticationService {

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public RegisterResponse register(@Valid RegisterRequest request) {
        log.info("Hello");
        var isExist = repository.findByUsername(request.getUsername()).isPresent();
        if (isExist) {
            throw new UserAlreadyExistException("User already exist");
        }

        var user = UserEntity.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .username(request.getUsername())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(request.getRole()).build();

        var userEntity = repository.save(user);
        return RegisterResponse.buildRegisterDto(userEntity);
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUsername(),
                        request.getPassword()
                )
        );
        var user = repository.findByUsername(request.getUsername())
                .orElseThrow(() -> new NoSuchUserException("User not found!"));

        var jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }
}
