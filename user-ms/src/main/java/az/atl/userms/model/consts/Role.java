package az.atl.userms.model.consts;

public enum Role {
    ADMIN,AGENT,SUPERVISOR,USER
}
