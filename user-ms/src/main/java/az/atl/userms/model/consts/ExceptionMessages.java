package az.atl.userms.model.consts;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ExceptionMessages {
    USER_NOT_FOUND("user.notFound"),INVALID_PASSWORD("invalid.password"),PASSWORD_NOT_MATCHES("passwordNotMatches");

    private final String message;
}
