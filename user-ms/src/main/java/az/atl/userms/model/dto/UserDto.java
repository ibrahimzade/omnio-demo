package az.atl.userms.model.dto;


import az.atl.userms.model.consts.Role;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDto {
    Long id;
    String name;
    String surname;
    String username;
    String email;
    String password;
    @Enumerated(EnumType.STRING)
    Role role;
}
