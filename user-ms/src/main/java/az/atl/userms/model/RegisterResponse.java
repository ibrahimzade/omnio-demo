package az.atl.userms.model;



import az.atl.userms.dao.entity.UserEntity;
import az.atl.userms.model.consts.Role;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegisterResponse {

    String name;
    String surname;
    String username;
    String email;
    String password;
    Role role;

    public static  RegisterResponse buildRegisterDto(UserEntity userEntity){
        return RegisterResponse.builder()
                .name(userEntity.getFirstName())
                .surname(userEntity.getLastName())
                .username(userEntity.getUsername())
                .email(userEntity.getEmail())
                .password(userEntity.getPassword())
                .role(userEntity.getRole()).build();
    }
}
