package az.atl.userms.model;

import az.atl.userms.model.consts.Role;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegisterRequest {
    @NotBlank(message = "FirstName is mandatory")
    @Size(min = 3 , message = "First name must be long 4 character at least")
    String firstName;
    @NotBlank(message = "LastName is mandatory")
    @Size(min = 4,message = "Last name must be long 4 character at least")
    String lastName;
    @NotBlank(message = "Username is mandatory")
    @Size(min = 4,message = "Username must be long 4 character at least")
    String username;
    @NotBlank(message = "Email is mandatory")
    String email;
    @NotBlank(message = "Password is mandatory")
    @Size(min = 6,message = "Password must be long 8 character at least")
    String password;
    Role role;
}

