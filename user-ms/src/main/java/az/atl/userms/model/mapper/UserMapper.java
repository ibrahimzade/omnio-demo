package az.atl.userms.model.mapper;

import az.atl.userms.dao.entity.UserEntity;
import az.atl.userms.model.dto.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(source = "firstName" , target = "name")
    @Mapping(source = "lastName" , target = "surname")
    UserDto buildEntityToDto(UserEntity userEntity);

    List<UserDto> buildEntityToDtoList(List<UserEntity> userEntity);

    @Mapping(source = "name" , target = "firstName")
    @Mapping(source = "surname" , target = "lastName")
    UserEntity buildDtoToEntity(UserDto userDto);
}
