package az.atl.userms.controller;

import az.atl.userms.dao.entity.UserEntity;
import az.atl.userms.dao.repository.UserRepository;
import az.atl.userms.exception.NoSuchUserException;
import az.atl.userms.model.MessageResponse;
import az.atl.userms.model.SendMessageRequest;
import az.atl.userms.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static az.atl.userms.model.consts.ExceptionMessages.USER_NOT_FOUND;

@RestController
@RequestMapping("/messages")
@AllArgsConstructor
public class MessageController {
    private final MessageService messageService;
    private final UserRepository userRepository;

    @PostMapping("/send")
    public ResponseEntity<String> sendMessage(
            @RequestBody SendMessageRequest request,
            @AuthenticationPrincipal UserDetails userDetails
    ) {
        UserEntity sender = userRepository.findByUsername(userDetails.getUsername())
                .orElseThrow(() -> new NoSuchUserException(USER_NOT_FOUND.getMessage()));

        messageService.sendMessage(request, sender);
        return ResponseEntity.ok("Message sent successfully");
    }

    @GetMapping("/get")
    public ResponseEntity<List<MessageResponse>> getMessages(
            @AuthenticationPrincipal UserDetails userDetails
    ) {
        UserEntity user = userRepository.findByUsername(userDetails.getUsername())
                .orElseThrow(() -> new NoSuchUserException(USER_NOT_FOUND.getMessage()));

        List<MessageResponse> messages = messageService.getMessages(user.getId());
        return ResponseEntity.ok(messages);
    }
}

