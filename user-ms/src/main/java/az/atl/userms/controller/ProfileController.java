package az.atl.userms.controller;

import az.atl.userms.model.dto.UpdateDto;
import az.atl.userms.model.dto.UserDto;
import az.atl.userms.service.ProfileService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/profile")
public class ProfileController {
    private final ProfileService profileService;

    @GetMapping("/all")
    public Page<UserDto> getAllUsers(Pageable pageable)   {
        return profileService.getAllUsers(pageable);
    }

    @GetMapping("/byId/{id}")
    public UserDto getUserById(@PathVariable("id") Long id) {
        return profileService.getUserById(id);
    }

    @PostMapping("/update")
    public void updateUser(@RequestBody UserDto userDto) {
        profileService.updateUser(userDto);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteUserById(@PathVariable("id") Long id) {
        profileService.deleteUserById(id);
    }
    @PostMapping("/change-password")
    public void changePassword(
            @RequestBody UpdateDto updateDto
    ) {
        profileService.changePassword(updateDto);
    }
}
