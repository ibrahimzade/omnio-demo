package az.atl.userms.dao.repository;

import az.atl.userms.dao.entity.MessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<MessageEntity,Long> {
    List<MessageEntity> findBySender_IdOrReceiver_Id(Long senderId, Long receiverId);
}
