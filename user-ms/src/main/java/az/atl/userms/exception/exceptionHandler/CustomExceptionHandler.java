package az.atl.userms.exception.exceptionHandler;

import az.atl.userms.exception.NoSuchUserException;
import az.atl.userms.exception.UserAlreadyExistException;
import az.atl.userms.model.dto.ExceptionDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class CustomExceptionHandler {
    @ExceptionHandler(value = NoSuchUserException.class)
    public ExceptionDto handleNoSuchUserException(NoSuchUserException noSuchUserException) {
        log.error("not found Exception {}", noSuchUserException.getMessage());
        return new ExceptionDto(HttpStatus.NOT_FOUND.value(), noSuchUserException.getMessage());
    }

    @ExceptionHandler(value = UserAlreadyExistException.class)
    public ExceptionDto handleUserAlreadyExistException(UserAlreadyExistException userAlreadyExistException) {
        log.error("already exist Exception {}", userAlreadyExistException.getMessage());
        return new ExceptionDto(HttpStatus.BAD_REQUEST.value(), userAlreadyExistException.getMessage());
    }

}
