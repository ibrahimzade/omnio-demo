package az.atl.userms.exception;

public class PasswordNotMatchesException extends RuntimeException{
    public PasswordNotMatchesException(String message) {
        super(message);
    }
}
